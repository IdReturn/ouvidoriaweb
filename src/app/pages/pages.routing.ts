import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ManifestationsComponent } from './manifestations/manifestations.component';
import { ManifestationComponent } from './manifestation/manifestation.component';
import { ManifestationResponseComponent } from './manifestation-response/manifestation-response.component';

export const PagesRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'manifestations', component: ManifestationsComponent },
  { path: 'manifestation-response', component: ManifestationResponseComponent },
  { path: 'manifestation', component: ManifestationComponent }
];
