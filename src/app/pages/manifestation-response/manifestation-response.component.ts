import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-manifestation-response',
  templateUrl: './manifestation-response.component.html',
  styleUrls: ['./manifestation-response.component.scss']
})
export class ManifestationResponseComponent implements OnInit, OnDestroy {

  public responseForm: FormGroup;
  public manifestationForm: FormGroup;
  public paramsSubscribe: Subscription;

  constructor (
    private router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.responseForm = this._formBuilder.group({
      year: 2018,
      sequence: 1,
      manifestationText: '',
      answerLetter: ''
    });

    this.paramsSubscribe = this.route.queryParams.subscribe(
      (queryParams: any) => {
        let year: number = queryParams['year'];
        let sequence: number = queryParams['sequence'];

        this.responseForm.setValue({
          year: year,
          sequence: sequence,
          manifestationText: 'manifestação text area',
          answerLetter: ''
        });
      }
    );
  }

  ngOnDestroy() {
    this.paramsSubscribe.unsubscribe();
  }

  onSubmit() {
    console.log(this.responseForm.value);
  }

}
