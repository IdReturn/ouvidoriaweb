import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

// Models
import { Manifestation } from '../../models/manifestation';

// Services
import { ManifestationService } from '../../services/manifestation.service';

// Validators
import { MyErrorStateMatcher, AppValidator } from '../../util';

import images from './imagens';

@Component({
  selector: 'app-manifestation',
  templateUrl: './manifestation.component.html',
  styleUrls: ['./manifestation.component.scss']
})
export class ManifestationComponent implements OnInit, OnDestroy {

  public manifestationForm: FormGroup;
  public paramsSubscribe: Subscription;

  matcher = new MyErrorStateMatcher();

  public imageSources: string[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private manifestationService: ManifestationService
  ) { this.imageSources = images; }

  ngOnInit() {
    this.manifestationForm = this._formBuilder.group({
      year: [null, Validators.required],
      sequence: [null, Validators.required],
      protocol: [null, Validators.required],
      dateIncluded: [null, Validators.required],
      userId: [null, Validators.required],
      classification: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      segment: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      place: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      subject: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      subSubject: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      photos: [[]],
      situation: [0, Validators.required],
      manifestationText: [''],
      answerLetter: ['', Validators.required],
      demand: this._formBuilder.group({
        id: this._formBuilder.control(0),
        description: this._formBuilder.control('')
      }),
      registration: [0, Validators.required],
      manifestationTypeId: [0],
      priorityId: [0]
    });

    this.paramsSubscribe = this.route.queryParams.subscribe(
      (queryParams: any) => {
        let year: number = queryParams['year'];
        let sequence: number = queryParams['sequence'];

        this.manifestationService.getManifestation(year, sequence)
          .then((res: Manifestation) => {
            res['protocol'] = res.year.toString() + res.sequence.toString();
            res.dateIncluded = res.dateIncluded.substring(0, 10);
            this.manifestationForm.setValue(res);
            this.imageSources.concat(res.photos.map(p => p.photo));
          })
          .catch((err) => {
            console.log(err);
            this.imageSources = [];
          });
      }
    );

  }

  ngOnDestroy() {
    this.paramsSubscribe.unsubscribe();
  }

}
